# Task 1

You need to create a simple web form with two field which get longitude and latitude from user.
Then you have to send it to Google Maps API and display a map (800x800px) and set pointer to place provided by user.
If you have a pointer you need to get some information about weather from https://openweathermap.org

### The informations which you need put into table are:

* Weather main ex. "Clear"
* Current temperature
* Current humidity

| Field | Value |
| ------ | ------ |
| Weather main | [Clear] |
| Current temp. | [23C] |
| Current humidity | [100%] |
